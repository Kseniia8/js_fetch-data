async function fetchFilms() {
    const response = await fetch('https://ajax.test-danit.com/api/swapi/films');
    const films = await response.json();
    return films;
}

async function fetchCharacter(url) {
    const response = await fetch(url);
    const character = await response.json();
    return character;
}

function createLoader() {
    const loader = document.createElement('div');
    loader.className = 'loader';
    return loader;
}

function renderFilm(film) {
    const filmElement = document.createElement('div');
    filmElement.innerHTML = `
        <h2>Episode ${film.episodeId}: ${film.name}</h2>
        <p>${film.openingCrawl}</p>
    `;
    
    const loader = createLoader();
    filmElement.appendChild(loader);
    
    const charactersList = document.createElement('ul');
    film.characters.forEach(characterUrl => {
        const characterItem = document.createElement('li');
        characterItem.textContent = 'Loading...';
        charactersList.appendChild(characterItem);
        fetchCharacter(characterUrl).then(character => {
            characterItem.textContent = character.name;
            loader.remove(); 
        });
    });
    
    filmElement.appendChild(charactersList);
    return filmElement;
}

async function renderFilms() {
    const films = await fetchFilms();
    const filmsContainer = document.getElementById('films');
    films.forEach(film => {
        const filmElement = renderFilm(film);
        filmsContainer.appendChild(filmElement);
    });
}

document.addEventListener('DOMContentLoaded', renderFilms);
